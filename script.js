// Single Source of Truth

const multi_form_data = {}

let page_count = 0

// Get DOM elements

const sideBarElement = document.getElementById("side-bar")
const rightHalfDiv = document.getElementById("right-half-div")
const nextStepButton = document.getElementById("next-step");

const stepOneForm = document.getElementById("step-1-form")
const stepTwoForm = document.getElementById("step-2-form")
const stepThreeForm = document.getElementById("step-3-form")
const stepFourForm = document.getElementById("step-4-form")
const stepFiveForm = document.getElementById("step-5-form")



const forms_html_data = [
                          stepOneForm, 
                          stepTwoForm,
                          stepThreeForm,
                          stepFourForm,
                          stepFiveForm
                        ]

stepTwoForm.style.display = 'none'
stepThreeForm.style.display = 'none'
stepFourForm.style.display = 'none'
stepFiveForm.style.display = 'none'


// Functions

// Toggle divs in the right half div
// For next button

const toggleDIVS = ( currentDivId , nextDivId ) => {

        for (let step = 0; step < forms_html_data.length; step++) {
                const formContainer = forms_html_data[step];
                if ( step === currentDivId ){
                   formContainer.style.display = 'none'
                } else if (step === nextDivId) {
                    formContainer.style.display = 'block'
                }
        }

}

const handleNextStep = (nextStepButtonEvent) => {
        const currentDivId = parseInt(nextStepButtonEvent.id, 10);
        const nextDivId = currentDivId + 1;
        
        toggleDIVS(currentDivId, nextDivId);
}

// For goBack button

const togglePreviousDIV = (event, goBackButton) => {
        event.preventDefault()

        const currentDivId = parseInt(goBackButton.parentElement.id, 10)
        const previousDivId = currentDivId - 1

        toggleDIVS( currentDivId, previousDivId )

        page_count -=1
        do_the_color_change()
}


const collectUserData = () => {
        const myFirstForm = document.getElementById('myFirstForm');
        const nameInput = document.getElementById('name');
        const emailInput = document.getElementById('email');
        const phoneNumberInput = document.getElementById('phone-number');
        

        if (myFirstForm.checkValidity()) {

            return {
                'name': nameInput.value,
                'email': emailInput.value,
                'phone-number': phoneNumberInput.value,
            }
        } else {
            alert('Please fill in all the details.')
            return false
        }

}
    
    

const collectStepOneData = (event, nextStepButtonEvent) => {

        event.preventDefault()

        if ( collectUserData() ) {
                multi_form_data['user'] = collectUserData()
                handleNextStep( nextStepButtonEvent )

                page_count+=1
                do_the_color_change()
        }

}


const collectSubscriptionData = () => {
        const selectedSubscription = document.querySelector('input[name="subscription"]:checked');
        
            const subscriptionType = selectedSubscription.dataset.type
            const subscriptionPrice = selectedSubscription.dataset.price
            const billingFrequency = selectedSubscription.dataset.frequency

    
            return {
                        'type': subscriptionType,
                        'price': subscriptionPrice,
                        'billingFrequency': billingFrequency
                   }
            
};
    


const collectStepTwoData = (event, nextStepButtonEvent) => {

        event.preventDefault()

        multi_form_data['subscriptionData'] = collectSubscriptionData()

        handleNextStep(nextStepButtonEvent.parentElement)

        page_count+=1;
        do_the_color_change()
    };



const addonsArray = () => {

      const addonsData = []
      const checkboxes = document.querySelectorAll('input[type="checkbox"]')

      checkboxes.forEach(checkbox => {
        if (checkbox.checked) {
            const addonName = checkbox.dataset.addon
            const addonPrice = checkbox.dataset.price

            if (addonName && addonPrice) {
                addonsData.push({
                    name: addonName,
                    price: addonPrice,
                });
            }
        }
        })

        return addonsData
}


const displayPrices = (data) => {

        document.getElementById('sub-type').textContent = data.subscriptionData.type;
        document.getElementById('sub-price').textContent = data.subscriptionData.price;

data.addons.forEach((addon, index) => {
    const addonNameElement = document.getElementById(`service-${index + 1}`);
    const addonPriceElement = document.getElementById(`service-${index + 1}-price`);

    if (addonNameElement && addonPriceElement) {
        addonNameElement.textContent = addon.name;
        addonPriceElement.textContent = addon.price;
    }
});

const elementsToHide = 3 - data.addons.length;

for (let i = data.addons.length; i <= elementsToHide; i++) {
    const addonNameElement = document.getElementById(`service-${i}`);
    const addonPriceElement = document.getElementById(`service-${i}-price`);

    if (addonNameElement && addonPriceElement) {
        addonNameElement.style.display = 'none';
        addonPriceElement.style.display = 'none';
    }
}


const calculateTotalPrice = (subscriptionData, addons, planType) => {
        let total = parseFloat(subscriptionData.price.replace(/[^\d.]/g, '')); 


        addons.forEach(addon => {
            total += parseFloat(addon.price.replace(/[^\d.]/g, ''));
        });
    
        return  planType === 'yearly' ? `$${total.toFixed(2)}/yr`: `$${total.toFixed(2)}/mo`
}


const totalElement = document.getElementById('total-price');
const totalPrice = calculateTotalPrice(data.subscriptionData, data.addons, data.subscriptionData.billingFrequency);
totalElement.textContent = totalPrice;



}



const collectStepThreeData = (nextStepButtonEvent) => {

        const addonsData = addonsArray()
  
        multi_form_data['addons'] = addonsData
    
        handleNextStep( nextStepButtonEvent.parentElement )

        displayPrices(multi_form_data)

        page_count+=1
        do_the_color_change()
} 


const total_bill_price = (bills) => {
   return bills.reduce((total_bill, bill) => total_bill + bill, 0)
}


const toggleButton = document.querySelector('.toggle-button');
const yearlyPlans = document.querySelectorAll('.yearly');
const monthlyPlans = document.querySelectorAll('.monthly');

const charges_one = document.getElementById('charges-1')
const charges_two = document.getElementById('charges-2')
const charges_three = document.getElementById('charges-3')


const onlineServiceCheckbox = document.getElementById('online-service');
const largerStorageCheckbox = document.getElementById('larger-storage');
const customizableProfileCheckbox = document.getElementById('customizable-profile');




const switchCheckbox = document.querySelector('.switch input');

switchCheckbox.addEventListener('change', () => {

    yearlyPlans.forEach(plan => {
        if (plan.style.display === 'none') {
            plan.style.display = 'block';

        } else {
            plan.style.display = 'none';
        }
    });

    if (Array.from(yearlyPlans).some((plan) => plan.style.display === 'block')) {
        charges_one.textContent = '$10/yr'
        charges_two.textContent = '$20/yr'
        charges_three.textContent = '$20/yr'

        onlineServiceCheckbox.setAttribute('data-price', '$10/yo');
        largerStorageCheckbox.setAttribute('data-price', '$20/yo');
        customizableProfileCheckbox.setAttribute('data-price', '$20/yo');

    } else {
        charges_one.textContent = '$1/mo'
        charges_two.textContent = '$2/mo'
        charges_three.textContent = '$2/mo'

        onlineServiceCheckbox.setAttribute('data-price', '$1/mo');
        largerStorageCheckbox.setAttribute('data-price', '$2/mo');
        customizableProfileCheckbox.setAttribute('data-price', '$2/mo');
    }
    
    monthlyPlans.forEach(plan => {
        if (plan.style.display === 'block') {
            plan.style.display = 'none';
        } else {
            plan.style.display = 'block';
        }
    });
});

function do_the_color_change(){
    document.querySelectorAll('.center-step').forEach((each_step,index) => {

        let children = each_step.querySelector('.round-number')

        if(index == page_count){
            children.className += ' change_the_color'
        }else{
            if(children.className.includes('change_the_color'))
            children.className = children.className.slice(0, -17)
        }
    })
}
do_the_color_change()

